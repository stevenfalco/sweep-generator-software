// Sweep Generator Arduino Code
// Copyright Dick Whipple 2020
// Version 1.0 (Nuts & Volts Final)
// 10 April 2021
//
// Copyright (c) 2023 Steven A. Falco

#include "APP.h"
#include "LED.h"

static APP *pApp;

void
setup() {
	Serial.begin(115200);

	Serial.print(F("Hello World\r\n"));

	pinMode(SW_LED, OUTPUT);
	digitalWrite(SW_LED, HIGH);

	pApp = new APP;

	// Initialize the app.  This also enables interrupts.
	pApp->begin();
}

void
loop()
{
	pApp->run();
}

ISR(PCINT0_vect)
{
	pApp->isrA();
}

ISR(PCINT1_vect)
{
	pApp->isrB();
}

ISR(PCINT2_vect)
{
	pApp->isrC();
}

ISR(PCINT3_vect)
{
	pApp->isrD();
}
