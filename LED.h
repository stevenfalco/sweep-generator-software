// Copyright 2023 Steven A. Falco <stevenfalco@gmail.com>
//
// This file is part of sweep_generator.
//
//  sweep_generator is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  sweep_generator is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with sweep_generator.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __LED__
#define __LED__

// We have a single LED connected to digital pin 10, which in the Sanguino
// mapping of an ATmega1284P is bit 2 of port D (i.e. PD2).
#define SW_LED		10

#endif // __LED__
