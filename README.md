WSAB - A Vintage Radio Sweep Generator
======================================

**_WSAB_** is a sweep alignment generator that is useful when restoring antique
radios.  The project was created by Richard Whipple, and more information is
available for Richard's original project at the following links:

https://www.nutsvolts.com/magazine/article/build-a-vintage-radio-sweep-alignment-instrument

https://www.nutsvolts.com/magazine/article/build-a-vintage-radio-sweep-alignment-instrument-part-2

Richard also wrote a book on the topic, available from Amazon:

https://www.amazon.com/Vintage-Radio-Alignment-Scratch/dp/B0933KLP5W

The sweep generator has AM and FM modes, and the software is open-source, so
it can easily be customized.

I decided to build a WSAB and this repository is my version of the software
that runs on the ATmega (Arduino clone).

My version of the companion hardware and mechanical design can be found in
https://gitlab.com/stevenfalco/sweep-generator-hardware.

I chose to 3D print a chassis, and I also used an inkjet printer to create a
front panel on photo paper.  The photo is glued to the chassis and the holes
are then cut in the paper with a razor knife.  Here is what the final project
looks like:

<img src="screenshots/generator1.jpg" >

It is not easy to photograph the unit in operation, but here is what the
display looks like:

<img src="screenshots/generator2.jpg" >

Richard has much better display photos in his magazine articles.  :-)

My build uses surface mounted parts (SMD), and you will have to have good
soldering skills and tools to duplicate this version.  But hey, I managed, and
you might want to try your hand at such a project.

All the CAD software that I used is also open-source (FreeCAD to desigh the 3D
case, Inkscape and GIMP to lay out the front panel artwork, and KiCad to create
the schematic and PCB "gerber" files.  I had OSH Park make the bare PCBs, and I
hand-soldered the components.

All of the design files from FreeCAD, KiCad, etc. are included in this
repository, in case you wish to tweak anything.

My version of the project uses an AD9851 Direct Digital Synthesis (DDS) chip,
rather than the more common AD9850.  The AD9851 runs at 180 MHz internally, and
can generate a clean 70 MHz sinewave output with a 7 stage reconstruction
filter.

In comparison, the earlier AD9850 chip runs at 125 MHz internally, and tops
out at about 40 MHz sinewave output.

Either chip type is sufficient for the WSAB, since FM IF strips typically run
at 10.7 MHz, but the price difference between the chips is small, so I went for
the faster ones.  Here is a scope trace showing the 70 MHz sinewave output,
which looks very clean:

<img src="screenshots/scope_trace.png" >

