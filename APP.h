// Sweep Generator Arduino Code
// Copyright Dick Whipple 2020
// Version 1.0 (Nuts & Volts Final)
// 10 April 2021
//
// Copyright 2023 Steven A. Falco <stevenfalco@gmail.com>
//
// This file is part of sweep_generator.
//
//  sweep_generator is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  sweep_generator is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with sweep_generator.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __APP__
#define __APP__

#include <Arduino.h>
#include <stdint.h>

#include <avr/io.h>

#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>

// https://github.com/brianlow/Rotary
//
// We have two versions of this library.
//
// ~/Arduino/libraries/Rotary-master/Rotary.cpp has a separate Rotary::begin()
// function, which sets the pin mode based on a "pullup" parameter.
//
// ~/arduino/rotary_encoder/arduino/libraries/Rotary/Rotary.cpp sets the pin
// mode in its constructor, and uses a #define for ENABLE_PULLUPS.
//
// Our code assumes the former version and calls the begin() function.  It
// also calls pinMode in our setup function.
//
#include <Rotary.h>

#include "AD9850.h"

// Pin definitions for the TFT display.  We rely on the standard Arduino
// definitions of SCK, MOSI, and MISO.
#define D13_TFT_CS			13
#define D14_TFT_RESET			14
#define D15_TFT_D_Cn			15		// D15_TFT_D/Cn on schematic

// Pin definitions for rotary encoder REN1 and REN2.  The names are weird
// because CLK and DATA are really just quadrature signals.
#define A7_FSTEP_CLK			A7
#define A6_FSTEP_DATA			A6
#define A3_FSTEP_SWITCH			A3
#define D11_FREQ_CLK			11
#define D12_FREQ_DATA			12
#define A4_FREQ_SWITCH			A4

// Pin definitions for option and mode push buttons.
#define D0_OPTION			0
#define D1_MODE				1

// Pin definition for the audio in from the unit under test.
#define AF_IN				A0

#define DEBOUNCE			10		// milliseconds

// Definitions for the TFT display.
//
// The upper left corner is (0, 0) and the lower right corner is
// (WIDTH - 1, HEIGHT - 1).
//
// We have two text regions, one at the top of the display and one at
// the bottom, with a graph inbetween.
//
// The graph runs from GRAPH_TOP_Y to GRAPH_BOTTOM_Y on the Y axis.
//
// Our Atmel chip has an A/D converter that gives us an output in the range
// of 0 to 1023, and we use that range for our internal units.  We then use
// the map() function to convert from our internal units to "display lines",
// which run from GRAPH_BOTTOM_Y to GRAPH_TOP_Y.
//
// Note that while the display line numbers increase towards the bottom of
// the display, our amplitude numbers increase towards the top of the display.

#define WIDTH				(128)
#define HALF_WIDTH			(WIDTH / 2)
#define HEIGHT				(160)
#define MIN_X				(0)
#define MIN_Y				(0)
#define MAX_X				(WIDTH - 1)
#define MAX_Y				(HEIGHT - 1)
#define X_MARGIN			(1)
#define Y_MARGIN			(1)
// Nominally 5x7 but there is a  1 pel descender, so call it 5x8
#define CHARACTER_WIDTH			(5)
#define CHARACTER_HEIGHT		(8)
#define CHARACTER_PAD_X			(1)
#define CHARACTER_PAD_Y			(1)
#define TEXT_HEIGHT			(CHARACTER_HEIGHT + CHARACTER_PAD_Y)

// We are using a 5x7 font and there is one pel between adjacent
// characters.  Thus, we can fit 21 characters per line (21 x 6 = 126).
#define CHARS_PER_LINE			int(WIDTH / (CHARACTER_WIDTH + CHARACTER_PAD_X))

// The ATmega A/D converter has 10-bit resolution.
#define AMPLITUDE_MIN			(0)		// Minimum amplitude
#define AMPLITUDE_MID			(512)		// Half amplitude
#define AMPLITUDE_MAX			(1023)		// Maxumum amplitude

// Vertical position.
#define VERT_POSITION_MIN		(-AMPLITUDE_MAX)
#define VERT_POSITION_MAX		(AMPLITUDE_MAX)
#define VERT_POSITION_INCR		(50)

// Gain settings.
#define GAIN_MIN			(0)
#define GAIN_MAX			(5)
#define GAIN_LAST			(GAIN_MAX + 1)
#define GAIN_SCALE			(100)

// Upper text area size.
#define UPPER_TEXT_WIDTH		(WIDTH)
#define UPPER_TEXT_HEIGHT		(Y_MARGIN + CHARACTER_HEIGHT + CHARACTER_PAD_Y + CHARACTER_HEIGHT + Y_MARGIN)

// Lower text area size.
#define LOWER_TEXT_WIDTH		(WIDTH)
#define LOWER_TEXT_HEIGHT		(Y_MARGIN + CHARACTER_HEIGHT + CHARACTER_PAD_Y + CHARACTER_HEIGHT + CHARACTER_PAD_Y + CHARACTER_HEIGHT + Y_MARGIN)

// Graph area size.
#define GRAPH_WIDTH			(WIDTH)
#define GRAPH_HEIGHT			(HEIGHT - UPPER_TEXT_HEIGHT - LOWER_TEXT_HEIGHT)

// Upper text line positions.
#define UPPER_TEXT_START_X		(MIN_X)
#define UPPER_TEXT_START_Y		(MIN_Y)
#define UPPER_TEXT_LINE_1		(UPPER_TEXT_START_Y + Y_MARGIN)
#define UPPER_TEXT_LINE_2		(UPPER_TEXT_LINE_1 + TEXT_HEIGHT)

// Definitions for the graph area.
// 
// The text areas have fixed heights, and the graphics get what is left over.
#define GRAPH_START_X			(MIN_X)
#define GRAPH_START_Y			(UPPER_TEXT_START_Y + UPPER_TEXT_HEIGHT)
#define GRAPH_START_X_MARGINS		(GRAPH_START_X + X_MARGIN)
#define GRAPH_START_Y_MARGINS		(GRAPH_START_Y + Y_MARGIN)
#define GRAPH_WIDTH_MARGINS		(GRAPH_WIDTH - (2 * X_MARGIN))
#define GRAPH_HEIGHT_MARGINS		(GRAPH_HEIGHT - (2 * Y_MARGIN))
// Active area of the graph on Y axis.
#define GRAPH_TOP_Y			(GRAPH_START_Y + Y_MARGIN)			// line 20
#define GRAPH_BOTTOM_Y			(GRAPH_START_Y + GRAPH_HEIGHT - Y_MARGIN)	// line 131

// Lower text line positions.
#define LOWER_TEXT_START_X		(MIN_X)
#define LOWER_TEXT_START_Y		(GRAPH_START_Y + GRAPH_HEIGHT)
#define LOWER_TEXT_LINE_1		(LOWER_TEXT_START_Y + Y_MARGIN)
#define LOWER_TEXT_LINE_2		(LOWER_TEXT_LINE_1 + TEXT_HEIGHT)
#define LOWER_TEXT_LINE_3		(LOWER_TEXT_LINE_2 + TEXT_HEIGHT)

#define SIGN_ON_Y			25

// For converting units as described above.  We provide a little margin
// at the top and bottom of the graph to aid readability.
#define MAP_MARGIN_Y			1
#define MAP_FULL			m_vertPositionCurr + AMPLITUDE_MIN + m_gainValue, m_vertPositionCurr + AMPLITUDE_MAX - m_gainValue, GRAPH_BOTTOM_Y - MAP_MARGIN_Y, GRAPH_TOP_Y + MAP_MARGIN_Y
#define MAP_HALF			m_vertPositionCurr + m_ampMinValue, m_vertPositionCurr + AMPLITUDE_MID - m_gainValue, GRAPH_BOTTOM_Y - MAP_MARGIN_Y, GRAPH_TOP_Y + MAP_MARGIN_Y

// Colors for the display.
#define TO_565(r, g, b) ((((r) & 0xf8) << 8) | (((g) & 0xfc) << 3) | (((b) & 0xf8) >> 3))
#define BLACK				TO_565(0, 0, 0)
#define GRAY				TO_565(127, 127, 127)
#define WHITE				TO_565(255, 255, 255)
#define RED				TO_565(255, 0, 0)
#define GREEN				TO_565(0, 255, 0)
#define BLUE				TO_565(0, 0, 255)
#define DK_BLUE				TO_565(0, 0, 180)
#define YELLOW				TO_565(255, 255, 0)
#define CYAN				TO_565(0, 255, 255)
#define MAGENTA				TO_565(255, 0, 255)

// Frequency limits.
#define MIN_FREQUENCY			0
#define MAX_FREQUENCY			70000000

// Sweep modes.
#define SWEEP_MODE_AM_IF_NEG		0
#define SWEEP_MODE_AM_IF_POS		1
#define SWEEP_MODE_FM_IF_NEG		2
#define SWEEP_MODE_FM_IF_POS		3
#define SWEEP_MODE_FM_SINGLE_S		4
#define SWEEP_MODE_FM_DOUBLE_S		5
#define SWEEP_MODE_LAST			6

// Operational modes.
#define OP_MODE_SWEEP_MODE		0
#define OP_MODE_SET_CENTER_FREQUENCY	1
#define OP_MODE_SET_SWEEP_WIDTH		2
#define OP_MODE_SET_LOWER_MARKER	3
#define OP_MODE_SET_UPPER_MARKER	4
#define OP_MODE_LAST			5

// Step sizes.
#define STEP_1_HZ			0
#define STEP_5_HZ			1
#define STEP_10_HZ			2
#define STEP_50_HZ			3
#define STEP_100_HZ			4
#define STEP_500_HZ			5
#define STEP_1_kHZ			6
#define STEP_5_kHZ			7
#define STEP_10_kHZ			8
#define STEP_50_kHZ			9
#define STEP_100_kHZ			10
#define STEP_500_kHZ			11
#define STEP_1_MHZ			12
#define STEP_5_MHZ			13
#define STEP_LAST			14

#define AM_IF_CENTER			455000
#define AM_IF_SWEEP_WIDTH		30000
#define AM_MARKER_OFFSET		5000

#define FM_IF_CENTER			10700000
#define FM_IF_SWEEP_WIDTH		400000
#define FM_S_CURVE_SWEEP_WIDTH		600000
#define FM_MARKER_OFFSET		100000

// Scaling
#define KILO				1000
#define MEGA				1000000

// Plot/erase
#define PLOTTING			0
#define ERASING				1

class APP
{
	public:
		APP();

		void begin();
		void run();

		void isrA();
		void isrB();
		void isrC();
		void isrD();

	private:
		// Objects
		AD9850			m_ad9850;
		Adafruit_ST7735		m_tftScreen;
		Rotary			m_freqEncoder;
		Rotary			m_freqStepEncoder;

		// Variables
		String			m_dbMarkerFreqLowerText;
		String			m_dbMarkerFreqUpperText;
		String			m_dbMarkerFreqVarText;
		String			m_markerFreqAmpText;
		String			m_markerFreqBalText;
		String			m_markerFreqZeroText;
		String			m_units;

		double			m_sqrRoot2;

		int			m_ampMaxValue;
		int			m_ampMaxIndex;
		int32_t			m_ampMaxFreq;
		int			m_ampMinValue;
		int			m_sModeInitialValue;
		int			m_ampBWLowerIndex;
		int			m_ampBWUpperIndex;
		int			m_ampData[WIDTH];
		int			m_ampMarkerFreqLowerIndex;
		int			m_ampMarkerFreqUpperIndex;
		int			m_ampMarkerFreqVarIndex;
		int			m_noiseFloor;
		volatile int		m_stepPointer;
		int			m_stepPointerPrev;
		int			m_gain;
		int			m_gainPrev;
		int			m_gainValue;

		volatile int		m_opMode;
		volatile int		m_opModePrev;
		volatile int		m_opModeState;
		volatile uint32_t	m_opModeTime;

		volatile int		m_sweepMode;
		volatile int		m_sweepModePrev;
		volatile int		m_sweepModeState;
		volatile uint32_t	m_sweepModeTime;

		volatile int		m_freqStepButtonState;
		volatile uint32_t	m_freqStepButtonTime;

		volatile uint32_t	m_freqButtonState;
		volatile uint32_t	m_freqButtonTime;

		// Use signed values so our ISR can check for underflow.
		volatile int32_t	m_freq;
		int32_t			m_freqBW;
		int32_t			m_freqCenter;
		int32_t			m_freqMarkerLower;
		int32_t			m_freqMarkerUpper;
		int32_t			m_freqPrev;
		int32_t			m_incr;

		int32_t			m_sweepWidth;
		int32_t			m_sweepWidthHalf;
		int32_t			m_sweepWidthPerPel;
		double			m_sweepWidthScaled;

		volatile int		m_vertPosition;
		int			m_vertPositionPrev;
		int			m_vertPositionCurr;

		// Functions
		void debug(
				String x,
				int line
			  );

		String getStepText(
				int step
				);

		int32_t getStep(
				int step
				);

		String getSweepMode(
				int index
				);

		inline void updateSweepWidth(
				int32_t freq
				)
		{
			m_sweepWidth = freq;
			m_sweepWidthHalf = freq / 2;
			m_sweepWidthPerPel = freq / WIDTH;
			m_sweepWidthScaled = WIDTH / (double)freq;
		}

		inline void updateStepSize(
				int step
				)
		{
			m_stepPointerPrev = m_stepPointer = step;
			m_incr = getStep(step);
			m_units = getStepText(step);
		}

		void opModeSweep();
		void setCenterFrequency();
		void setSweepWidth();
		void setLowerMarker();
		void setUpperMarker();

		void checkStep(
				String msg
				);

		void displayUpdateUpperFrame(
				String msg
				);

		String lineFormat(
				uint32_t freqTemp0,
				uint32_t freqTemp1,
				uint32_t freqTemp2,
				int type
				);

		void displayLowerFrame(
				String line1Text,
				String line2Text,
				String line3Text
				);

		void displayUpperFrame(
				String line1Text,
				String line2Text
				);

		void clearGraph();

		void doPlot(
				int mode
				);

		void clearLowerFrame();
		void clearUpperFrame();
};

#endif // __APP__
