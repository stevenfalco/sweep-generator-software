// Copyright 2023 Steven A. Falco <stevenfalco@gmail.com>
//
// This file is part of sweep_generator.
//
//  sweep_generator is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  sweep_generator is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with sweep_generator.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __AD9850__
#define __AD9850__

#include <Arduino.h>
#include <stdint.h>

#include <avr/io.h>

// Pin definitions for AD9850.  The data bus is on Port C.  We will use the
// Sanguino mapping, i.e. Arduino D16:D23 map to OSC_DATA[0..7].
#define OSC_CLOCK	2
#define OSC_STROBE	3
#define OSC_RESET	4
#define OSC_DATA_DIR	DDRC
#define OSC_DATA_RW	PORTC

#ifdef DDS_IS_AD9850
// AD9850 uses a high-speed clock, and does not have a built-in multiplier.
#define DDS_CLOCK	125.0e6	// We use a 125 MHz oscillator.
#define DDS_W0		0x00	// No multiplier and the LSB must be 0.
#elif DDS_IS_AD9851
// AD9851 uses a lower-speed clock, and has a built-in 6X multiplier.
#define DDS_CLOCK	180.0e6	// 30 MHz x 6 is the effective clock speed.
#define DDS_W0		0x01	// LSB must be 1 to enable the multiplier.
#else
#error "You must define either AD9850 or AD9851"
#endif


class AD9850
{
	public:
		AD9850();

		void setFrequency(double frequency);

	private:
		void pulseHigh(uint8_t pin);

		void transferByte(uint8_t data);
};

#endif // __AD9850__
