// Sweep Generator Arduino Code
// Copyright Dick Whipple 2020
// Version 1.0 (Nuts & Volts Final)
// 10 April 2021
//
// Copyright 2023 Steven A. Falco <stevenfalco@gmail.com>
//
// This file is part of sweep_generator.
//
//  sweep_generator is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  sweep_generator is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with sweep_generator.  If not, see <https://www.gnu.org/licenses/>.

#include "APP.h"

APP::APP() :
	m_ad9850(),
	m_tftScreen(D13_TFT_CS, D15_TFT_D_Cn, D14_TFT_RESET),

	// Note - there are different brands of encoders, and if yours
	// increase values when turned counter-clockwise, you can fix
	// that by defining or undefining REVERSE_ROTATION as needed.
#define REVERSE_ROTATION

#ifdef REVERSE_ROTATION
	m_freqEncoder(D12_FREQ_DATA, D11_FREQ_CLK),
	m_freqStepEncoder(A6_FSTEP_DATA, A7_FSTEP_CLK)
#else
	m_freqEncoder(D12_FREQ_CLK, D11_FREQ_DATA),
	m_freqStepEncoder(A6_FSTEP_CLK, A7_FSTEP_DATA)
#endif
{	
	//Serial.print(F("APP constructor begins\r\n"));

	// Initial frequency.
	m_freq = AM_IF_CENTER;

	// Save the frequency - this lets us detect changes.
	m_freqPrev = m_freq;

	// Center frequency for sweep.
	m_freqCenter = AM_IF_CENTER;

	// Width of sweep.  We keep several related values so we don't
	// have to keep recalculating them.
	updateSweepWidth(AM_IF_SWEEP_WIDTH);

	// Lower marker frequency (below center frequency)
	m_freqMarkerLower = m_freqCenter - AM_MARKER_OFFSET;

	// Upper marker frequency (above center frequency)
	m_freqMarkerUpper = m_freqCenter + AM_MARKER_OFFSET;

	// Width at -3db frequencies based on amplitude at center frequency
	m_freqBW = 0;

	// Initial frequency step size.
	updateStepSize(STEP_1_kHZ);

	// Initial operational mode.
	m_opMode = OP_MODE_SWEEP_MODE;
	m_opModePrev = OP_MODE_LAST;
	m_opModeState = -1;

	// Initial sweep mode.
	m_sweepMode = SWEEP_MODE_AM_IF_NEG;
	m_sweepModePrev = SWEEP_MODE_LAST;
	m_sweepModeState = -1;

	// Initial extra buttons.
	m_freqStepButtonState = -1;
	m_freqButtonState = -1;

	m_noiseFloor = 0;
	m_gain = GAIN_MIN;
	m_gainPrev = GAIN_LAST;

	// For finding -3 dB points.
	m_sqrRoot2 = sqrt(2);

	m_vertPosition = 0;
	m_vertPositionPrev = 0;
	m_vertPositionCurr = 0;
}

void
APP::debug(
		String x,
		int line
		)
{
	m_tftScreen.setCursor(X_MARGIN, SIGN_ON_Y + (TEXT_HEIGHT * line));
	m_tftScreen.print(x);
}

// Translate the step pointer to a label.
String
APP::getStepText(
		int step
		)
{
	switch(step) {
		case STEP_1_HZ:		return "1 Hz";
		case STEP_5_HZ:		return "5 Hz";
		case STEP_10_HZ:	return "10 Hz";
		case STEP_50_HZ:	return "50 Hz";
		case STEP_100_HZ:	return "100 Hz";
		case STEP_500_HZ:	return "500 Hz";
		case STEP_1_kHZ:	return "1 kHz";
		case STEP_5_kHZ:	return "5 kHz";
		case STEP_10_kHZ:	return "10 kHz";
		case STEP_50_kHZ:	return "50 kHz";
		case STEP_100_kHZ:	return "100 kHz";
		case STEP_500_kHZ:	return "500 kHz";
		case STEP_1_MHZ:	return "1 MHz";
		case STEP_5_MHZ:	return "5 MHz";
		default:		return "?";
	}
}

// Translate the step pointer to actual frequency increment.
int32_t
APP::getStep(
		int step
	    )
{
	switch(step) {
		case STEP_1_HZ:		return 1;
		case STEP_5_HZ:		return 5;
		case STEP_10_HZ:	return 10;
		case STEP_50_HZ:	return 50;
		case STEP_100_HZ:	return 100;
		case STEP_500_HZ:	return 500;	
		case STEP_1_kHZ:	return 1000;
		case STEP_5_kHZ:	return 5000;
		case STEP_10_kHZ:	return 10000;
		case STEP_50_kHZ:	return 50000;
		case STEP_100_kHZ:	return 100000;
		case STEP_500_kHZ:	return 500000;
		case STEP_1_MHZ:	return 1000000;
		case STEP_5_MHZ:	return 5000000;
		default:		return 0;
	}
}

// Translate the sweep mode into a string.
String
APP::getSweepMode(
		int index
		)
{
	// "AM IF(-)"  - AM w/ negative going sweep voltage (most tube radios)
	// "AM IF(+)"  - AM w/ postive going sweep voltage (most transistor radios)
	// "FM IF(-)"  - FM IF alignment w/ negative going sweep voltage
	// "FM IF(+)"  - FM IF alignment w/ positive going sweep voltage
	// "FM SS(+-)" - Single S-curve w/ positive and negative going sweep voltage
	// "FM DS(+-)" - Double S-curve w/ positive and negative going sweep voltage
	switch(index) {
		case SWEEP_MODE_AM_IF_NEG:	return("AM IF(-)kHz");
		case SWEEP_MODE_AM_IF_POS:	return("AM IF(+)kHz");
		case SWEEP_MODE_FM_IF_NEG:	return("FM IF(-)MHz");
		case SWEEP_MODE_FM_IF_POS:	return("FM IF(+)kHz");
		case SWEEP_MODE_FM_SINGLE_S:	return("FM SS(+-)MHz");
		case SWEEP_MODE_FM_DOUBLE_S:	return("FM DS(+-)MHz");
		default:			return("?");
	}
}

// We divide the display into three frames.  At the top and bottom we have
// two lines of text in each frame, and in the middle we have a graph.

// Clear the upper text frame.
void
APP::clearUpperFrame()
{
	m_tftScreen.fillRect(UPPER_TEXT_START_X, UPPER_TEXT_START_Y, WIDTH, UPPER_TEXT_HEIGHT, WHITE);
}

// Display text in the upper text frame.
//
// We map to two lines.
void
APP::displayUpperFrame(
		String line1Text,
		String line2Text
		)
{
	clearUpperFrame();

	m_tftScreen.setCursor(X_MARGIN, UPPER_TEXT_LINE_1);
	m_tftScreen.print(line1Text);

	m_tftScreen.setCursor(X_MARGIN, UPPER_TEXT_LINE_2);
	m_tftScreen.print(line2Text);
}

// Update the text in the upper frame.
void
APP::displayUpdateUpperFrame(
		String msg
		)
{
	String freqText;
       
	if(m_freq < KILO) {
		freqText = msg + String(double(m_freq)) + " Hz";
	} else if(m_freq < MEGA) {
		freqText = msg + String(double(m_freq) / KILO) + " kHz";
	} else {
		freqText = msg + String(double(m_freq) / MEGA) + " MHz";
	}

	String unitsText = "Step Size: " + m_units;

	displayUpperFrame(freqText, unitsText);
}

// Clear the lower text frame.
void
APP::clearLowerFrame()
{
	m_tftScreen.fillRect(LOWER_TEXT_START_X, LOWER_TEXT_START_Y, WIDTH, LOWER_TEXT_HEIGHT, WHITE);
}

// Display text in the lower text frame.
//
// We map to three fields.
void
APP::displayLowerFrame(
		String line1Text,
		String line2Text,
		String line3Text
		)
{
	clearLowerFrame();

	m_tftScreen.setCursor(X_MARGIN, LOWER_TEXT_LINE_1);
	m_tftScreen.print(line1Text);

	m_tftScreen.setCursor(X_MARGIN, LOWER_TEXT_LINE_2);
	m_tftScreen.print(line2Text);

	m_tftScreen.setCursor(X_MARGIN, LOWER_TEXT_LINE_3);
	m_tftScreen.print(line3Text);
}

// Clear everything in the graph frame, and redraw the markers.
//
// We cannot use this at the start of each sweep, because it causes too much
// flicker, and we don't have enough ram to use an off-screen bitmap.
void
APP::clearGraph()
{
	int32_t frequency_offset = (m_freqCenter - m_sweepWidthHalf);

	// Draw the graph background.
	m_tftScreen.drawRect(GRAPH_START_X, GRAPH_START_Y, GRAPH_WIDTH, GRAPH_HEIGHT, GRAY);
	m_tftScreen.fillRect(GRAPH_START_X_MARGINS, GRAPH_START_Y_MARGINS, GRAPH_WIDTH_MARGINS, GRAPH_HEIGHT_MARGINS, DK_BLUE);

	// Initialize the lower frequency marker position.  This represents
	// the ideal lower bound of the passband.  It is the full height
	// of the graph, to the left of center.
	m_ampMarkerFreqLowerIndex = (double)(m_freqMarkerLower - frequency_offset) * m_sweepWidthScaled;

	// Initialize the upper frequency marker position.  This represents
	// the ideal upper bound of the passband.  It is the full height
	// of the graph, to the right of center.
	m_ampMarkerFreqUpperIndex = (double)(m_freqMarkerUpper - frequency_offset) * m_sweepWidthScaled;

	// Initialize the user-selectable variable marker position.
	m_ampMarkerFreqVarIndex = (double)(m_freq - frequency_offset) * m_sweepWidthScaled;
}

// Plot or erase.  We assume that most of the background and fixed lines are
// intact from the last call to clearGraph().
//
// We have saved indices of the markers and saved amplitude data, so we can
// erase those items.
void
APP::doPlot(
		int mode
		)
{
	int x;
	int i;
	int value;
	int plotColor;
	int markerColor3dB;
	int markerColorVar;

	if(mode == ERASING) {
		plotColor = DK_BLUE;
		markerColor3dB = DK_BLUE;
		markerColorVar = DK_BLUE;
	} else {
		plotColor = YELLOW;
		markerColor3dB = RED;
		markerColorVar = GREEN;
	}

	// Whether we are plotting or erasing we must redraw any lines that
	// might have been overwritten.
	//
	// Here we redraw the left and right edges of the graph as well as
	// the center line.
	//
	// We do this before plotting points, because we want the points
	// to take precedence.
	m_tftScreen.drawLine(MIN_X, GRAPH_TOP_Y, MIN_X, GRAPH_BOTTOM_Y, YELLOW);
	m_tftScreen.drawLine(MAX_X, GRAPH_TOP_Y, MAX_X, GRAPH_BOTTOM_Y, YELLOW);
	m_tftScreen.drawLine(HALF_WIDTH, GRAPH_TOP_Y, HALF_WIDTH, GRAPH_BOTTOM_Y, YELLOW);

	// S-curves have a horizontal line at mid-screen.
	int hLine = map(AMPLITUDE_MID, MAP_FULL);
	if(m_sweepMode == SWEEP_MODE_FM_SINGLE_S || m_sweepMode == SWEEP_MODE_FM_DOUBLE_S) {
		m_tftScreen.drawLine(MIN_X, hLine, MAX_X, hLine, YELLOW);
	}

	// Update the lower frequency marker position.  This represents
	// the ideal lower bound of the passband.  It is the full height
	// of the graph, to the left of center.
	m_ampMarkerFreqLowerIndex = (double)(m_freqMarkerLower - (m_freqCenter - m_sweepWidthHalf)) * m_sweepWidthScaled;
	m_tftScreen.drawLine(m_ampMarkerFreqLowerIndex, GRAPH_TOP_Y, m_ampMarkerFreqLowerIndex, GRAPH_BOTTOM_Y, YELLOW);

	// Update the upper frequency marker position.  This represents
	// the ideal upper bound of the passband.  It is the full height
	// of the graph, to the right of center.
	m_ampMarkerFreqUpperIndex = (double)(m_freqMarkerUpper - (m_freqCenter - m_sweepWidthHalf)) * m_sweepWidthScaled;
	m_tftScreen.drawLine(m_ampMarkerFreqUpperIndex, GRAPH_TOP_Y, m_ampMarkerFreqUpperIndex, GRAPH_BOTTOM_Y, YELLOW);

	// Now that the static lines have been plotted we can move on to
	// handling the curves and movable markers.
	//
	// Most tube radios develop negative AGC voltages at resonance.  The
	// original code assumed that the AGC voltage would vary between 0
	// and some negative value.  But there are radios that have a
	// slightly positive AGC voltage when far from resonance.
	//
	// For our transistor radio kit, the voltage at the volume control
	// rests at about 600 mV, and at peak response goes down to about
	// -2.0 V.
	//
	// I've changed the mapping to put the 
	for(x = MIN_X; x <= MAX_X; x++) {
		// map(...) converts from one range to another.  See the
		// definitions of MAP_FULL and MAP_HALF in APP.h
		//
		// constrain(...) limits the first arg to be between the
		// second and third args.
		//
		// Convert the measured voltage to a display row.
		if(m_sweepMode == SWEEP_MODE_FM_DOUBLE_S) {
			value = constrain(map(m_ampData[x] - m_sModeInitialValue, MAP_FULL), GRAPH_TOP_Y, GRAPH_BOTTOM_Y);
		} else if(m_sweepMode == SWEEP_MODE_FM_SINGLE_S) {
			value = constrain(map(m_ampData[x], MAP_FULL), GRAPH_TOP_Y, GRAPH_BOTTOM_Y);
		} else {
			// Non-S-curve modes.
			value = constrain(map(m_ampData[x], MAP_HALF), GRAPH_TOP_Y, GRAPH_BOTTOM_Y);
		}

		if(m_sweepMode == SWEEP_MODE_FM_DOUBLE_S) {
			// In Double S mode, we plot two points in each column.
			// Thus, each point gets plotted twice.
			//
			// Plot the first point for this column.
			m_tftScreen.drawPixel(x, value, plotColor);

			// Plot the second point for this column.
			value = constrain(map(m_ampData[MAX_X - x] - m_sModeInitialValue, MAP_FULL), GRAPH_TOP_Y, GRAPH_BOTTOM_Y);
			m_tftScreen.drawPixel(x, value, plotColor);
		} else {
			// All other modes just plot one point per column.
			m_tftScreen.drawPixel(x, value, plotColor);
		}

		// Plot/erase the -3 dB markers.  These don't apply to S-curves.
		if((x == m_ampBWLowerIndex || x == m_ampBWUpperIndex) && (m_sweepMode < SWEEP_MODE_FM_SINGLE_S)) {
			for(i = -3; i < 4; i++) {
				m_tftScreen.drawPixel(x, value + i, markerColor3dB);
			}
		}

		// Plot/erase the user-selectable variable frequency marker.
		if(x == m_ampMarkerFreqVarIndex) {
			for(i = -5; i < 6; i++) {
				m_tftScreen.drawPixel(x, value + i, markerColorVar);
			}
		}
	}
}

void
APP::opModeSweep()
{
	int x;
	int value;
	int i;
	int ampAtPeakFreq;
	int32_t offset;
	int32_t freq;

	// See if there has been a change that forces redrawing.
	if(m_opMode != m_opModePrev || m_gain != m_gainPrev || m_vertPosition != m_vertPositionPrev) {
		// Yes - we have to repaint the whole graph.
		m_opModePrev = m_opMode;
		m_gainPrev = m_gain;
		m_vertPositionPrev = m_vertPosition;
		clearGraph();
	} else {
		// Not starting from scratch, so clear the previous plotted
		// data.  Ideally we'd always start from scratch, but that
		// would cause too much flicker.  Also, we don't have enough
		// ram for an off-screen buffer.
		doPlot(ERASING);
	}

	// debug("p " + String(m_vertPosition) + "    ", 0);

	// Measure the noise floor at 1/5 of the center frequency.
	m_ad9850.setFrequency(m_freqCenter / 5);
	delayMicroseconds(150);

	m_sModeInitialValue = 0;
	if(m_sweepMode == SWEEP_MODE_AM_IF_NEG || m_sweepMode == SWEEP_MODE_FM_IF_NEG) {
		m_noiseFloor = (AMPLITUDE_MID - analogRead(AF_IN));
		if(m_vertPosition > 0) {
			m_vertPositionPrev = m_vertPosition = 0;
		}
	} else if(m_sweepMode == SWEEP_MODE_AM_IF_POS || m_sweepMode == SWEEP_MODE_FM_IF_POS) {
		m_noiseFloor = (analogRead(AF_IN) - AMPLITUDE_MID);
		if(m_vertPosition > 0) {
			m_vertPositionPrev = m_vertPosition = 0;
		}
	} else {
		// S-curve modes.
		m_noiseFloor = 0;
		m_sModeInitialValue = analogRead(AF_IN) - AMPLITUDE_MID;
	}
	m_vertPositionCurr = m_vertPosition / 2;

	// Measure response curve amplitudes.  We make one measurement per
	// display column, as that is the best resolution we can achieve.
	//
	// As we do this, keep track of the highest value we've seen and
	// where it is located.
	m_ampMaxValue = AMPLITUDE_MIN;
	m_ampMinValue = AMPLITUDE_MAX;
	for(x = MIN_X; x <= MAX_X; x++) {
		// Calculate and set the next frequency.  Allow a little time
		// for the DDS chip's output to settle and for the radio to
		// respond.
		offset = ((long)(x - HALF_WIDTH)) * m_sweepWidthPerPel;
		freq = m_freqCenter + offset;
		m_ad9850.setFrequency(freq);
		delayMicroseconds(150);

		// Read and store the value at this point.
		if(m_sweepMode == SWEEP_MODE_AM_IF_NEG || m_sweepMode == SWEEP_MODE_FM_IF_NEG) {
			value = (AMPLITUDE_MID - analogRead(AF_IN));
		} else if(m_sweepMode == SWEEP_MODE_AM_IF_POS || m_sweepMode == SWEEP_MODE_FM_IF_POS) {
			value = (analogRead(AF_IN) - AMPLITUDE_MID);
		} else {
			// S-curve modes.
			value = analogRead(AF_IN);
		}
		m_ampData[x] = value;

		if(value > m_ampMaxValue) {
			m_ampMaxValue = value;
			m_ampMaxIndex = x;
			m_ampMaxFreq = freq;
		}

		if(value < m_ampMinValue) {
			m_ampMinValue = value;
		}
	}
	ampAtPeakFreq = m_ampMaxValue - m_noiseFloor;

	// Scan for the -3 dB point below the center frequency.
	// We scan from left to right.
	m_ampBWLowerIndex = 0;
	for(i = 0; i < m_ampMaxIndex; i++) {
		if((ampAtPeakFreq - m_sqrRoot2 * (m_ampData[i] - m_noiseFloor)) <= 0) {
			m_ampBWLowerIndex = i;
			break;
		}
	}

	// Scan for the -3 dB point above the center frequency.
	// We scan from right to left.
	m_ampBWUpperIndex = MAX_X;
	for(i = m_ampBWUpperIndex; i > m_ampMaxIndex; i--) {
		if((ampAtPeakFreq - m_sqrRoot2 * (m_ampData[i] - m_noiseFloor)) <= 0) {
			m_ampBWUpperIndex = i;
			break;
		}
	}

	// Update the user-selectable variable marker position.
	m_ampMarkerFreqVarIndex = (double)(m_freq - (m_freqCenter - m_sweepWidthHalf)) * m_sweepWidthScaled;

	// Update the gain value.
	m_gainValue = m_gain * GAIN_SCALE;

	doPlot(PLOTTING);

	// Update upper text frame.
	String freqSpreadText = lineFormat((m_freqCenter - m_sweepWidthHalf), m_freqCenter, (m_freqCenter + m_sweepWidthHalf), 1);
	displayUpperFrame("Swp " + getSweepMode(m_sweepMode) + " G:" + String(m_gain + 1), freqSpreadText);

	// Update lower text frame.
	if(m_sweepMode < SWEEP_MODE_FM_SINGLE_S) {
		// Not in S-curve mode.

		// Create "peak frequency" string.
		String freqAtMaxText;
		if(m_ampMaxFreq < MEGA) {
			freqAtMaxText = "PF:" + String(m_ampMaxFreq / KILO) + "kHz";
		} else {
			freqAtMaxText = "PF:" + String((double)m_ampMaxFreq / MEGA) + "MHz";
		}

		// Convert the markers to strings in dBv.  We scale up by
		// 200.0, round off to the nearest integer, then scale down
		// by 10.0.  That should give us one decimal place, although
		// we may have to truncate to make everything fit.
		m_dbMarkerFreqLowerText = String(round(200.0 * log10((double)(m_ampData[m_ampMarkerFreqLowerIndex] - m_noiseFloor) / ampAtPeakFreq)) / 10.0);
		m_dbMarkerFreqUpperText = String(round(200.0 * log10((double)(m_ampData[m_ampMarkerFreqUpperIndex] - m_noiseFloor) / ampAtPeakFreq)) / 10.0);
		m_dbMarkerFreqVarText   = String(round(200.0 * log10((double)(m_ampData[m_ampMarkerFreqVarIndex]   - m_noiseFloor) / ampAtPeakFreq)) / 10.0);

		// If the amplitude is too low, then m_ampBWUpperIndex and
		// m_ampBWLowerIndex may be at the edges of the screen.  In
		// that case, the bandwidth is indeterminate, so substitute
		// zero.
		m_freqBW = 0;
		if(m_ampBWLowerIndex > MIN_X && m_ampBWUpperIndex < MAX_X) {
			m_freqBW = ((long)(m_ampBWUpperIndex - m_ampBWLowerIndex) * m_sweepWidthPerPel) / KILO;
		}

		displayLowerFrame(
				// Line 0.
				lineFormat(m_freqMarkerLower, m_freq, m_freqMarkerUpper, 0),
				// Line 1.  We manually distribute the fields,
				// knowing we can fit 21 characters on a line.
				m_dbMarkerFreqLowerText.substring(0, 4) + "dB  " + m_dbMarkerFreqVarText.substring(0, 4) + "dB " + m_dbMarkerFreqUpperText.substring(0, 4) + "dB",
				// Line 2.
				freqAtMaxText + " BW:" + String(m_freqBW) + "kHz");
	} else {
		// In S-curve mode.
		m_markerFreqAmpText  = String((m_ampData[m_ampMarkerFreqUpperIndex] - m_ampData[m_ampMarkerFreqLowerIndex]) / 2);
		m_markerFreqBalText  = String( m_ampData[m_ampMarkerFreqUpperIndex] + m_ampData[m_ampMarkerFreqLowerIndex] - AMPLITUDE_MAX);
		m_markerFreqZeroText = String( m_ampData[HALF_WIDTH] - AMPLITUDE_MID);

		displayLowerFrame(
				// Line 0.
				lineFormat(m_freqMarkerLower, m_freq, m_freqMarkerUpper, 0),
				// Line 1.
				"Amp: " + m_markerFreqAmpText + " Bal: " + m_markerFreqBalText,
				// Line 2.
				"Zero: " + m_markerFreqZeroText);
	}

	// Check for sweep mode change.  This happens via an ISR.
	if(m_sweepMode != m_sweepModePrev) {
		m_sweepModePrev = m_sweepMode;

		int32_t newWidth;
		int32_t newStep;
		switch(m_sweepMode) {
			case SWEEP_MODE_AM_IF_NEG:
			case SWEEP_MODE_AM_IF_POS:
				m_freq = AM_IF_CENTER;
				m_freqCenter = m_freq;
				newWidth = AM_IF_SWEEP_WIDTH;
				m_freqMarkerLower = AM_IF_CENTER - AM_MARKER_OFFSET;
				m_freqMarkerUpper = AM_IF_CENTER + AM_MARKER_OFFSET;
				newStep = STEP_1_kHZ;
				break;

			case SWEEP_MODE_FM_IF_NEG:
			case SWEEP_MODE_FM_IF_POS:
				m_freq = FM_IF_CENTER;
				m_freqCenter = m_freq;
				newWidth = FM_IF_SWEEP_WIDTH;
				m_freqMarkerLower = FM_IF_CENTER - FM_MARKER_OFFSET;
				m_freqMarkerUpper = FM_IF_CENTER + FM_MARKER_OFFSET;
				newStep = STEP_10_kHZ;
				break;

			case SWEEP_MODE_FM_SINGLE_S:
			case SWEEP_MODE_FM_DOUBLE_S:
				m_freq = FM_IF_CENTER;
				m_freqCenter = m_freq;
				newWidth = FM_S_CURVE_SWEEP_WIDTH;
				m_freqMarkerLower = FM_IF_CENTER - FM_MARKER_OFFSET;
				m_freqMarkerUpper = FM_IF_CENTER + FM_MARKER_OFFSET;
				newStep = STEP_10_kHZ;
				break;
		}

		m_vertPositionCurr = m_vertPositionPrev = m_vertPosition = 0;
		updateSweepWidth(newWidth);
		updateStepSize(newStep);
		clearGraph();
	}
}

void
APP::setCenterFrequency()
{
	if(m_opMode != m_opModePrev) {
		m_opModePrev = m_opMode;

		displayLowerFrame("Set Center Frequency", "", "");
		m_freqPrev = m_freq = m_freqCenter;
		m_ad9850.setFrequency(m_freq);
		displayUpdateUpperFrame("Center: ");

		clearGraph();
	}

	checkStep("Center: ");

	if(m_freqPrev != m_freq) {
		m_freqPrev = m_freqCenter = m_freq;
		m_ad9850.setFrequency(m_freq);
		delayMicroseconds(10);
		displayUpdateUpperFrame("Center: ");
	}
}

void
APP::setSweepWidth()
{
	if(m_opMode != m_opModePrev) {
		m_opModePrev = m_opMode;

		displayLowerFrame("   Set Sweep Width", "", "");
		m_freqPrev = m_freq = m_sweepWidth;
		m_ad9850.setFrequency(m_freqCenter);
		displayUpdateUpperFrame("Width: ");

		clearGraph();
	}

	checkStep("Width: ");

	if(m_freqPrev != m_freq) {
		m_freqPrev = m_freq;
		updateSweepWidth(m_freq);
		displayUpdateUpperFrame("Width: ");
	}
}

void
APP::setLowerMarker()
{
	if(m_opMode != m_opModePrev) {
		m_opModePrev = m_opMode;

		displayLowerFrame( "   Set Low Marker", "", "");
		m_freqPrev = m_freq = m_freqMarkerLower;
		m_ad9850.setFrequency(m_freq);
		displayUpdateUpperFrame("LoMark: ");

		clearGraph();
	}

	checkStep("LoMark: ");

	if(m_freqPrev != m_freq) {
		m_freqPrev = m_freqMarkerLower = m_freq;
		m_ad9850.setFrequency(m_freq);
		displayUpdateUpperFrame("LoMark: ");
	}
}

void
APP::setUpperMarker()
{
	if(m_opMode != m_opModePrev) {
		m_opModePrev = m_opMode;

		displayLowerFrame("   Set High Marker", "" ,"");
		m_freqPrev = m_freq = m_freqMarkerUpper;
		m_ad9850.setFrequency(m_freq);
		displayUpdateUpperFrame("HiMark: ");

		clearGraph();
	}

	checkStep("HiMark: ");

	if(m_freqPrev != m_freq) {
		m_freqPrev = m_freqMarkerUpper = m_freq;
		m_ad9850.setFrequency(m_freq);
		displayUpdateUpperFrame("HiMark: ");
	}
}

// React to step size changes.  We don't want this in the ISR because it
// is too slow.
void
APP::checkStep(
		String msg
		)
{
	if(m_stepPointer != m_stepPointerPrev) {
		m_stepPointerPrev = m_stepPointer;

		m_incr = getStep(m_stepPointer);
		m_units = getStepText(m_stepPointer);
		displayUpdateUpperFrame(msg);
	}
}

// Format text before displaying it.
String
APP::lineFormat(
		uint32_t f0,
		uint32_t f1,
		uint32_t f2,
		int type
	       )
{

	String fT0Text, fT1Text, fT2Text, spacesText = "          ";

	int fT0Len, fT1Len, fT2Len, spaces;

	if(f0 < MEGA) {
		// Scale to kilohertz.
		fT0Text = String(f0 / KILO);
		fT1Text = String(f1 / KILO);
		fT2Text = String(f2 / KILO);
	} else {
		// Scale to megahertz.
		fT0Text = String((double)f0 / MEGA, 3);
		fT1Text = String((double)f1 / MEGA, 2);
		fT2Text = String((double)f2 / MEGA, 3);
	}

	// Find string lengths, then find how many spaces we have room for.
	fT0Len = fT0Text.length();
	fT1Len = fT1Text.length();
	fT2Len = fT2Text.length();
	spaces = CHARS_PER_LINE - (fT0Len + fT1Len + fT2Len);

	switch(type) {
		case 0:
			// Type 0 spreads the spaces into four equal areas.
			return
				spacesText.substring(0, spaces / 4)
				+ fT0Text
				+ spacesText.substring(0, spaces / 4)
				+ fT1Text
				+ spacesText.substring(0, spaces / 4)
				+ fT2Text;

		case 1:
			// Type 1 spreads the spaces into two equal areas.
			return
				fT0Text
				+ spacesText.substring(0, spaces / 2)
				+ fT1Text
				+ spacesText.substring(0, spaces / 2)
				+ fT2Text;

		default:
			return "";
	}
}

void
APP::begin()
{
	int line;

	// Set up the frequency step encoder pins.
	pinMode(A7_FSTEP_CLK, INPUT_PULLUP);
	pinMode(A6_FSTEP_DATA, INPUT_PULLUP);

	// Set up the frequency encoder pins.
	pinMode(D11_FREQ_CLK, INPUT_PULLUP);
	pinMode(D12_FREQ_DATA, INPUT_PULLUP);

	// Set up the pushbuttons.
	pinMode(D1_MODE, INPUT_PULLUP);
	pinMode(D0_OPTION, INPUT_PULLUP);

	// Initialize the display.  It is oriented vertically and is 128 pels
	// wide by 160 pels tall.
	//
	// We double-space the sign-on lines.
	m_tftScreen.initR(INITR_BLACKTAB);
	m_tftScreen.setRotation(0);
	m_tftScreen.setTextSize(1);
	m_tftScreen.setTextWrap(true);
	m_tftScreen.fillRect(MIN_X, MIN_Y, WIDTH, HEIGHT, WHITE);
	m_tftScreen.setTextColor(BLACK, WHITE);

	line = 0;
	m_tftScreen.setCursor(X_MARGIN, SIGN_ON_Y + (TEXT_HEIGHT * line));
	m_tftScreen.print("        WSAB         ");
	line += 2;
	m_tftScreen.setCursor(X_MARGIN, SIGN_ON_Y + (TEXT_HEIGHT * line));
	m_tftScreen.print(" by Richard Whipple  ");
	line += 4;
	m_tftScreen.setCursor(X_MARGIN, SIGN_ON_Y + (TEXT_HEIGHT * line));
	m_tftScreen.print("   This version by   ");
	line += 2;
	m_tftScreen.setCursor(X_MARGIN, SIGN_ON_Y + (TEXT_HEIGHT * line));
	m_tftScreen.print("  Falco Engineering  ");
	line += 2;
	m_tftScreen.setCursor(X_MARGIN, SIGN_ON_Y + (TEXT_HEIGHT * line));
	m_tftScreen.print("     Version 1.1     ");
	delay(1000);
	clearUpperFrame();
	clearGraph();

	// Initialize the frequency encoders.
	m_freqStepEncoder.begin();
	m_freqEncoder.begin();			// Managed by ISR

	// Prepare our interrupts.  There are three levels of control:
	//
	// 1) cli/sei turn interrupts off/on globally
	// 2) PCICR turns off/on groups of 8 - i.e. all on a port
	// 3) PCMSKn turns off/on individual pin change interrupts
	//
	// Note that we don't rely on the interrupts to tell us which
	// order the pins might change in, because that is unreliable.
	// There is a state-machine in the Rotary package that sorts
	// out the changes.  All the interrupt tells us is that something
	// might have changed that we need to be aware of.
	//
	// Start by disabling all interrupts.
	cli();

	// Enable the bits we care about.
	PCMSK0 |= (1 << PCINT3) | (1 << PCINT4 | 1 << PCINT6 | 1 << PCINT7);
	PCMSK1 |= (1 << PCINT8) | (1 << PCINT9);
	PCMSK3 |= (1 << PCINT27) | (1 << PCINT28);

	// Enable the ports we care about.
	PCICR |= ((1 << PCIE0) | (1 << PCIE1) | (1 << PCIE3));

	// Enable interrupts.
	sei();
}

void
APP::run()
{
	// m_opMode updates via an ISR.
	switch(m_opMode) {
		case OP_MODE_SWEEP_MODE:		opModeSweep();		break;
		case OP_MODE_SET_CENTER_FREQUENCY:	setCenterFrequency();	break;
		case OP_MODE_SET_SWEEP_WIDTH:		setSweepWidth();	break;
		case OP_MODE_SET_LOWER_MARKER:		setLowerMarker();	break;
		case OP_MODE_SET_UPPER_MARKER:		setUpperMarker();	break;
		default:							break;
	}
}

void
APP::isrA()
{
	// Overflow takes about 50 days.  We don't need to worry about it.
	uint32_t currentTime = millis();

	// Read the current pushbutton states.  We don't know which pin(s)
	// (if any) might be the trigger.
	//
	// These are the pushbuttons built into the rotaries.  They are
	// named for the associated rotaries, but that has nothing to
	// do with their functions.
	int freqStepButton = digitalRead(A3_FSTEP_SWITCH);
	int freqButton = digitalRead(A4_FREQ_SWITCH);		// Gain.

	// Determine if enough time has elapsed to test this pin.
	if((currentTime - m_freqStepButtonTime) > DEBOUNCE) {
		// Enough time has elapsed, but this is a shared interrupt
		// so we want edges rather than levels.  See if this pin
		// has actually toggled.
		if(freqStepButton != m_freqStepButtonState) {
			// It toggled - note the new state and reset the
			// timer.
			m_freqStepButtonState = freqStepButton;
			m_freqStepButtonTime = currentTime;

			// We act on falling edges.
			if(freqStepButton == LOW) {
				// Force AM IF NEG.
				m_sweepMode = SWEEP_MODE_AM_IF_NEG;
			}
		}
	}

	// Same as above but without all the comments. :-)
	if((currentTime - m_freqButtonTime) > DEBOUNCE) {
		if(freqButton != m_freqButtonState) {
			m_freqButtonState = freqButton;
			m_freqButtonTime = currentTime;

			if(freqButton == LOW) {
				// Change the gain.
				if(++m_gain > GAIN_MAX) {
					m_gain = GAIN_MIN;
				}

			}
		}
	}

	switch(m_freqStepEncoder.process()) {
		case DIR_CW:
			switch(m_opMode) {
				case OP_MODE_SWEEP_MODE:
					// When in sweeep mode, this rotary
					// is used for vertical position.
					m_vertPosition -= VERT_POSITION_INCR;
					if(m_vertPosition < VERT_POSITION_MIN) {
						m_vertPosition = VERT_POSITION_MIN;
					}
					break;

				default:
					// When changing parameters, this
					// rotary is used to alter the step
					// size.
					if(m_stepPointer < STEP_5_MHZ) {
						m_stepPointer++;
					}
					break;
			}
			break;

		case DIR_CCW:
			switch(m_opMode) {
				case OP_MODE_SWEEP_MODE:
					// When in sweeep mode, this rotary
					// is used for vertical position.
					m_vertPosition += VERT_POSITION_INCR;
					if(m_vertPosition > VERT_POSITION_MAX) {
						m_vertPosition = VERT_POSITION_MAX;
					}
					break;

				default:
					// When changing parameters, this
					// rotary is used to alter the step
					// size.
					if(m_stepPointer > STEP_1_HZ) {
						m_stepPointer--;
					}
					break;
			}
			break;

		default:
			break;
	}
}

void
APP::isrB()
{
	// Overflow takes about 50 days.  We don't need to worry about it.
	uint32_t currentTime = millis();

	// Read the current pushbutton states.  We don't know which pin(s)
	// (if any) might be the trigger.
	//
	// These are the discrete pushbuttons (as opposed to the ones that
	// are on the rotaries).
	int opMode = digitalRead(D1_MODE);		// Change parameters.
	int sweepMode = digitalRead(D0_OPTION);		// Set AM / FM / etc.

	// Determine if enough time has elapsed to test this pin.
	if((currentTime - m_opModeTime) > DEBOUNCE) {
		// Enough time has elapsed, but this is a shared interrupt
		// so we want edges rather than levels.  See if this pin
		// has actually toggled.
		if(opMode != m_opModeState) {
			// It toggled - note the new state and reset the
			// timer.
			m_opModeState = opMode;
			m_opModeTime = currentTime;

			// We act on falling edges.
			if(opMode == LOW) {
				if(++m_opMode >= OP_MODE_LAST) {
					m_opMode = OP_MODE_SWEEP_MODE;
				}
			}
		}
	}

	// Same as above but without all the comments. :-)
	if((currentTime - m_sweepModeTime) > DEBOUNCE) {
		if(sweepMode != m_sweepModeState) {
			m_sweepModeState = sweepMode;
			m_sweepModeTime = currentTime;

			if(sweepMode == LOW) {
				if(++m_sweepMode >= SWEEP_MODE_LAST) {
					m_sweepMode = SWEEP_MODE_AM_IF_NEG;
				}
			}
		}
	}
}

void
APP::isrC()
{
}

void
APP::isrD()
{
	switch(m_freqEncoder.process()) {
		case DIR_CW:
			m_freq += m_incr;
			if(m_freq > MAX_FREQUENCY) {
				m_freq = MAX_FREQUENCY;
			}
			break;

		case DIR_CCW:
			m_freq -= m_incr;
			if(m_freq < MIN_FREQUENCY) {
				m_freq = MIN_FREQUENCY;
			}
			break;

		default:
			break;
	}
}
