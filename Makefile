# My local paths.
BASE_DIR		:= /home/sfalco/Arduino
PROJECT_DIR		:= $(BASE_DIR)/sketches/sweep_generator
ARDMK_DIR		:= $(BASE_DIR)/extras/Arduino-Makefile

# Some system paths.
ARDUINO_DIR		:= /opt/arduino-1.8.19
AVRDUDE			:= /usr/bin/avrdude

# Must be in $(BASE_DIR)/hardware to be found automatically.
ALTERNATE_CORE		:= MightyCore

# This must match a set of entries in MightyCore/avr/boards.txt.
BOARD_TAG		:= 1284

# There must be a pinout variant with this name.
VARIANT			:= sanguino

# MightyCore doesn't specify the exact MCU, so we need to provide it.
MCU			:= atmega1284p

# CPU oscillator frequency.
F_CPU			:= 20000000

# These are the libraries we need.  Arduino-Makefile will find them
# but I haven't found a way to specify which version we want.
ARDUINO_LIBS		:=				\
			   Adafruit_BusIO		\
			   Adafruit-GFX-Library		\
			   Adafruit-ST7735-Library	\
			   Rotary-master		\
			   SPI				\
			   Wire				\
			   #

# Using FTDI friend for upload:
MONITOR_BAUDRATE	:= 115200
MONITOR_PORT		:= /dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A104JTQP-if00-port0
AVRDUDE_ARD_PROGRAMMER	:= arduino
AVRDUDE_ARD_BAUDRATE	:= 115200
# AVRDUDE_OPTS		:= ""

# Using C232HM for upload:
# AVRDUDE_ARD_PROGRAMMER	:= C232HM
# AVRDUDE_ARD_BAUDRATE	:= 100000

# Fuses for our hardware configuration.
ISP_LOW_FUSE		:= 0xf7
ISP_HIGH_FUSE		:= 0xde
ISP_EXT_FUSE		:= 0xfd

# We have 2^17 bytes of flash, and 1024 bytes are used for the bootloader.
# This value is used when we do a "make upload", "make ispload", etc.
HEX_MAXIMUM_SIZE	:= 130048

CFLAGS_STD		:= -std=gnu11
CXXFLAGS_STD		:= -std=gnu++11

CXXFLAGS		+= -pedantic -Wall -Wextra

# Uncomment only one of the following:
#CXXFLAGS		+= -DDDS_IS_AD9850
CXXFLAGS		+= -DDDS_IS_AD9851

CURRENT_DIR		= $(shell basename $(CURDIR))

OBJDIR			= $(PROJECT_DIR)/bin/$(BOARD_TAG)/$(CURRENT_DIR)

include $(ARDMK_DIR)/Arduino.mk
