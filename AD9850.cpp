// Copyright 2023 Steven A. Falco <stevenfalco@gmail.com>
//
// This file is part of sweep_generator.
//
//  sweep_generator is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  sweep_generator is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with sweep_generator.  If not, see <https://www.gnu.org/licenses/>.

#include "AD9850.h"

AD9850::AD9850()
{
	//Serial.print(F("AD9850 constructor begins\r\n"));

	// Set up the AD9850 pins.  All bits are outputs; the device cannot
	// return any status.
	//
	// We access the data port as a byte rather than as individual bits,
	// for efficiency.
	pinMode(OSC_CLOCK, OUTPUT);
	pinMode(OSC_STROBE, OUTPUT);
	pinMode(OSC_RESET, OUTPUT);
	OSC_DATA_DIR = 0xff;

	// RESET must be at least 5 clocks wide.  The worst case is for an
	// AD9851-based board with a 30 MHz oscillator (33.3 ns period).
	// 5 x 33.3 ns is about 166 ns.
	//
	// digitalWrite takes about 3.4 us, according to one site I found,
	// so that would be the narrowest pulse we could possibly make.
	//
	// RESET clears the shift-register pointer, but does not clear the
	// shift-register itself.
	pulseHigh(OSC_RESET);

	// Set an initial frequency.  This initializes the input shift-register
	// thus avoiding any illegal codes.
	setFrequency(0);
}

void
AD9850::pulseHigh(uint8_t pin)
{
	digitalWrite(pin, HIGH);
	digitalWrite(pin, LOW);
}

void
AD9850::transferByte(uint8_t data)
{
	OSC_DATA_RW = data;
	pulseHigh(OSC_CLOCK);
}

void
AD9850::setFrequency(double frequency)
{
	// Convert the desired frequency to the proper register bits of the
	// DDS.  See pg 8 of the AD9850 datasheet.
	int32_t freq = (frequency * (1ull << 32)) / DDS_CLOCK;

	// Control byte.
	transferByte(DDS_W0);

	// Write the frequency value to W1-W4, MSB first.
	transferByte((freq >> 24) & 0xff);
	transferByte((freq >> 16) & 0xff);
	transferByte((freq >>  8) & 0xff);
	transferByte((freq >>  0) & 0xff);

	// Lock in the settings.  This also resets the internal device pointer
	// so we'll be ready to load W0 the next time we are called.
	pulseHigh(OSC_STROBE);
}
